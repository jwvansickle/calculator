//
//  Calculator.swift
//  Calculator
//
//  Created by John VanSickle on 12/8/14.
//  Copyright (c) 2014 John VanSickle. All rights reserved.
//

import Foundation

class Calculator {
    var firstNum: Int!;
    var secondNum: Int!;
    var mathSymbol: MathFunctions!;
    
    init(){
        
    }
    
    func solve() -> Int{
        var solution = 0;
        if (mathSymbol != nil && firstNum != nil && secondNum != nil){
        switch(mathSymbol.hashValue){
            case MathFunctions.ADDITION.hashValue:
                solution = firstNum + secondNum;
                break;
            case MathFunctions.SUBTRACTION.hashValue:
                solution = firstNum - secondNum;
                break;
            case MathFunctions.MULTIPLICATION.hashValue:
                solution = firstNum * secondNum;
                break;
            case MathFunctions.DIVISION.hashValue:
                solution = firstNum / secondNum;
                break;
            default:
                break;
        }
        }
        return solution;
    }
    
    func clear() {
        self.firstNum = nil;
        self.secondNum = nil;
        self.mathSymbol = nil;
    }
    
    enum MathFunctions {
        case ADDITION
        case SUBTRACTION
        case MULTIPLICATION
        case DIVISION
    }
}