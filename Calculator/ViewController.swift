//
//  ViewController.swift
//  Calculator
//
//  Created by John VanSickle on 12/8/14.
//  Copyright (c) 2014 John VanSickle. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // JWV : Additional setup
        self.textLabel.text = "";
        self.expressionLabel.text = "";
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var expressionLabel: UILabel!
    
    let calc = Calculator();
    
    @IBAction func append1() {
        self.textLabel.text = self.textLabel.text! + "1";
    }
    @IBAction func append2() {
        self.textLabel.text = self.textLabel.text! + "2";
    }
    @IBAction func append3() {
        self.textLabel.text = self.textLabel.text! + "3";
    }
    @IBAction func append4() {
        self.textLabel.text = self.textLabel.text! + "4";
    }
    @IBAction func append5() {
        self.textLabel.text = self.textLabel.text! + "5";
    }
    @IBAction func append6() {
        self.textLabel.text = self.textLabel.text! + "6";
    }
    @IBAction func append7() {
        self.textLabel.text = self.textLabel.text! + "7";
    }
    @IBAction func append8() {
        self.textLabel.text = self.textLabel.text! + "8";
    }
    @IBAction func append9() {
        self.textLabel.text = self.textLabel.text! + "9";
    }
    @IBAction func append0() {
        self.textLabel.text = self.textLabel.text! + "0";
    }
    @IBAction func beginAdd() {
        self.calc.firstNum = self.textLabel.text?.toInt();
        self.calc.mathSymbol = Calculator.MathFunctions.ADDITION;
        self.expressionLabel.text = self.textLabel.text! + "+";
        self.textLabel.text = "";
    }
    @IBAction func beginSub() {
        self.calc.firstNum = self.textLabel.text?.toInt();
        self.calc.mathSymbol = Calculator.MathFunctions.SUBTRACTION;
        self.expressionLabel.text = self.textLabel.text! + "-";
        self.textLabel.text = "";
    }
    @IBAction func beginMult() {
        self.calc.firstNum = self.textLabel.text?.toInt();
        self.calc.mathSymbol = Calculator.MathFunctions.MULTIPLICATION;
        self.expressionLabel.text = self.textLabel.text! + "*";
        self.textLabel.text = "";
    }
    @IBAction func beginDiv() {
        self.calc.firstNum = self.textLabel.text?.toInt();
        self.calc.mathSymbol = Calculator.MathFunctions.DIVISION;
        self.expressionLabel.text = self.textLabel.text! + "/";
        self.textLabel.text = "";
    }
    @IBAction func solve() {
        self.calc.secondNum = self.textLabel.text?.toInt();
        let solution = self.calc.solve();
        self.expressionLabel.text = "";
        self.textLabel.text = String(solution);
    }
    @IBAction func clear() {
        self.calc.clear();
        self.expressionLabel.text = "";
        self.textLabel.text = "";
    }
}

